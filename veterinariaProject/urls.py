from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from veterinariaApp import views

urlpatterns = [
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.ClienteCreateView.as_view()),
    path('user/<int:pk>/', views.ClienteDetailView.as_view()),
    path('user/update/<int:pk>/',views.ClienteUpdateView.as_view()),

    path('citas/<int:cliente_id>/', views.CitasCreateView.as_view()),
    path('citas/consult/<int:cliente_id>/', views.CitasDetailView.as_view()),
    path('citas/update/<int:cliente_id>/<int:pk>/', views.CitasUpdateView.as_view()),
    path('citas/remove/<int:cliente_id>/<int:pk>/', views.CitasDeleteView.as_view()),
    
]