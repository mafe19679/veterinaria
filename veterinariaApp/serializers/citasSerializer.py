from veterinariaApp.models.citas import Citas
from veterinariaApp.models.cliente import Cliente
from rest_framework import serializers


class CitasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Citas
        fields = ['fecha', 'hora', 'tipo_cita', 'nombre_veterinario','cliente']

    def to_representation(self, obj):
            citas = Citas.objects.get(id=obj.id)
            cliente = Cliente.objects.get(id=obj.cliente_id)
            return   {
                "id": citas.id,
                "fecha": citas.fecha,
                "hora": citas.hora,
                "tipo_cita": citas.tipo_cita,
                "nombre_veterinario": citas.nombre_veterinario,
                'cliente': {
                    'id':cliente.id,
                    'documento': cliente.documento,
                    'nombre': cliente.nombre,
                    'correo': cliente.correo,
                    'celular': cliente.celular,
                    'especie_mascota': cliente.especie_mascota,
                    'nombre_mascota': cliente.nombre_mascota,
                    'edad_mascota': cliente.edad_mascota,
                    'raza_mascota': cliente.raza_mascota,
                    'genero_mascota': cliente.genero_mascota

                }
            }
                

                