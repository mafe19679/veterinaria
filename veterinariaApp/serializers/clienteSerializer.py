from rest_framework import serializers
from veterinariaApp.models.cliente import Cliente
from veterinariaApp.models.citas import Citas
# from veterinariaApp.serializers.citasSerializer import CitasSerializer

class ClienteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Cliente
        fields = ['id','documento', 'nombre', 'correo', 'celular', 'especie_mascota', 'nombre_mascota', 'edad_mascota','raza_mascota','genero_mascota','password']

    def create(self, validated_data):
        # citasData = validated_data.pop('citas')
        clienteInstance = Cliente.objects.create(**validated_data)
        # Citas.objects.create(documento=clienteInstance, **citasData)
        return clienteInstance

    def to_representation(self, obj):
        cliente = Cliente.objects.get(id=obj.id)
        
        return {
                    'id': cliente.id,
                    'documento': cliente.documento,
                    'nombre': cliente.nombre,
                    'correo': cliente.correo,
                    'celular': cliente.celular,
                    'especie_mascota': cliente.especie_mascota,
                    'nombre_mascota': cliente.nombre_mascota,
                    'edad_mascota': cliente.edad_mascota,
                    'raza_mascota': cliente.raza_mascota,
                    'genero_mascota': cliente.genero_mascota
                }

