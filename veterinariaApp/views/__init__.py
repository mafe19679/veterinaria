
from .clienteCreateView import ClienteCreateView
from .clienteDetailView import ClienteDetailView, ClienteUpdateView
from .citasView import CitasCreateView, CitasDetailView,  CitasUpdateView, CitasDeleteView
# from .citasCreateView import CitasCreateView