from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated

from veterinariaApp.models.citas import Citas
from veterinariaApp.serializers.citasSerializer import CitasSerializer

from veterinariaApp.models.cliente import Cliente

class CitasDetailView(generics.ListAPIView): #__str__ returned non-string (type int)
    serializer_class = CitasSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        token = self.request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)
    
        if valid_data['cliente_id'] != self.kwargs['cliente_id']:
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        
        queryset = Citas.objects.filter(cliente_id=self.kwargs['cliente_id'])
        return queryset 

class CitasCreateView(generics.CreateAPIView): 
    serializer_class = CitasSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

    
        if valid_data['cliente_id'] != kwargs['cliente_id']:
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)

        serializer = CitasSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response("Cita Agendada correctamente", status=status.HTTP_201_CREATED)

class CitasUpdateView(generics.UpdateAPIView):
    serializer_class = CitasSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Citas.objects.all()

    def get(self, request, *args, **kwargs):
        print("Request:", request)
        print("Args:", args)
        print("kWargs:", kwargs)

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

    
        if valid_data['cliente_id'] != kwargs['cliente_id']:
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)

        return super().update(request, *args, **kwargs)    

class CitasDeleteView(generics.DestroyAPIView):
    serializer_class = CitasSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Citas.objects.all()

    def get(self, request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

    
        if valid_data['cliente_id'] != kwargs['cliente_id']:
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        else:
            super().destroy(*args, **kwargs)
            return Response({'message': 'Cita Eliminada'}, status=status.HTTP_401_UNAUTHORIZED)


       