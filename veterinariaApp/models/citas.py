from django.db import models
from veterinariaApp.models.cliente import Cliente

class Citas (models.Model):
    id = models.AutoField(primary_key=True)
    cliente =  models.ForeignKey (Cliente, related_name= 'citas', on_delete=models.CASCADE)
    fecha = models.DateField()
    hora = models.TimeField()
    tipo_cita = models.CharField (max_length= 50)
    nombre_veterinario = models.CharField (max_length = 50)

#def __str__(self):
 #   return str(self.documento)