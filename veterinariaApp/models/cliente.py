from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password

class UserManager(BaseUserManager):
    def create_user(self, documento, password=None):
        if not documento:
            raise ValueError('Users must have an username')
        user = self.model(documento=documento)
        user.set_password(password)
        user.save(using=self._db)
        return user
  
class Cliente(AbstractBaseUser, PermissionsMixin):
    id=models.BigAutoField(primary_key=True)
    documento= models.IntegerField(unique=True)
    nombre=models.CharField(max_length=50)
    correo=models.CharField(max_length=50)
    celular=models.IntegerField()
    especie_mascota=models.CharField(max_length=50)
    nombre_mascota=models.CharField(max_length=50)
    edad_mascota=models.IntegerField()
    raza_mascota=models.CharField(max_length=40)
    genero_mascota=models.CharField(max_length=20)
    password=models.CharField(max_length=256)

    def save(self, **kwargs):
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN'
        self.password = make_password(self.password, some_salt)
        super().save(**kwargs)

    objects = UserManager()
    USERNAME_FIELD = 'documento'
    
