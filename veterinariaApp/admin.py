from django.contrib import admin
from .models.cliente import Cliente
from .models.citas import Citas
from veterinariaApp.models import Cliente

admin.site.register(Cliente)
admin.site.register(Citas)